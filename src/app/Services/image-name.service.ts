import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageNameService {

  constructor() { }

  assetDic = {
    'BNB.BNB': 'https://chaosnet.bepswap.com/static/media/coin-bnb.25324922.svg',
    'BNB.FTM-A64': 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/FTM-A64/logo.png',
    'BNB.BUSD-BD1': 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/BUSD-BD1/logo.png',
    RUNE: '../../assets/img/Thorchain.jpg',
    'BNB.AVA-645': 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/AVA-645/logo.png',
    'BNB.TWT-8C2': 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/TWT-8C2/logo.png',
    'BNB.ETH-1C9': 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/ETH-1C9/logo.png',
    'BNB.BTCB-1DE': 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/BTCB-1DE/logo.png',
    'BNB.CAN-677': 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/CAN-677/logo.png',
    'BNB.BULL-BE4': 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/BULL-BE4/logo.png',
    'BNB.DOS-120': 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/DOS-120/logo.png',
    'BNB.SWINGBY-888': 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/SWINGBY-888/logo.png',
    'BNB.LTC-F07' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/LTC-F07/logo.png',
    'BNB.CBIX-3C9' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/CBIX-3C9/logo.png',
    'BNB.ERD-D06' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/ERD-D06/logo.png',
    'BNB.DARC-24B' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/DARC-24B/logo.png',
    'BNB.AWC-986' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/AWC-986/logo.png',
    'BNB.GIV-94E' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/GIV-94E/logo.png',
    'BNB.LIT-099' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/LIT-099/logo.png',
    'BNB.BCH-1FD' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/BCH-1FD/logo.png',
    'BNB.EOSBULL-F0D' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/EOSBULL-F0D/logo.png',
    'BNB.SHR-DB6' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/SHR-DB6/logo.png',
    'BNB.VIDT-F53' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/VIDT-F53/logo.png',
    'BNB.WISH-2D5' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/WISH-2D5/logo.png',
    'BNB.FRM-DE7' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/FRM-DE7/logo.png',
    'BNB.ETHBULL-D33' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/ETHBULL-D33/logo.png',
    'BNB.XRP-BF2' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/XRP-BF2/logo.png',
    'BNB.BEAR-14C' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/BEAR-14C/logo.png',
    'BNB.BZNT-464' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/BZNT-464/logo.png',
    'BNB.MITX-CAA' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/MITX-CAA/logo.png',
    'BNB.BOLT-4C6' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/BOLT-4C6/logo.png',
    'BNB.AERGO-46B' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/AERGO-46B/logo.png',
    'BNB.COTI-CBB' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/COTI-CBB/logo.png',
    'BNB.TOMOB-4BC' : 'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/assets/TOMOB-4BC/logo.png'
  };
//
//
//   [
//       "BNB.GIV-94E",
//   "BNB.MITX-CAA",
//   "BNB.BNB",
//   "BNB.DARC-24B",
//   "BNB.SWINGBY-888",
//   "BNB.XRP-BF2",
//   "BNB.ERD-D06",
//   "BNB.BEAR-14C",
//   "BNB.FTM-A64",
//   "BNB.AVA-645",
//   "BNB.DOS-120",
//   "BNB.BOLT-4C6",
//   "BNB.CAN-677",
//   "BNB.LTC-F07",
//   "BNB.LIT-099",
//   "BNB.EOSBULL-F0D",
//   "BNB.BZNT-464",
//   "BNB.BCH-1FD",
//   "BNB.AWC-986",
//   "BNB.ETHBULL-D33",
//   "BNB.SHR-DB6",
//   "BNB.BUSD-BD1",
//   "BNB.TWT-8C2",
//   "BNB.BTCB-1DE",
//   "BNB.ETH-1C9",
//   "BNB.BULL-BE4",
//   "BNB.PROPEL-6D9",
//   "BNB.VIDT-F53",
//   "BNB.CBIX-3C9"
// ]
//
  getImage(name) {
    return this.assetDic[name] !== undefined ? this.assetDic[name] : '../../assets/img/gray.png';
  }

}
