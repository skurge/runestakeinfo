import { Injectable } from '@angular/core';
import {ApiService} from './api-service.service';
import {UtilsService} from './utils.service';

@Injectable({
  providedIn: 'root'
})
export class UiServiceService {

  constructor(private utils: UtilsService) { }


  getColumnClass(count: number){
    if (count <= 1){
      return 'col-lg-8 col-md-8 ml-auto mr-auto';
    }else if (count === 2){
      return 'col-lg-6 col-md-6 ml-auto mr-auto';
    }else {
      return 'col-lg-4 col-md-6 ml-auto mr-auto';
    }
  }

  getBaseTotalSummary(asset, baseTicker: string){
    switch (baseTicker){
      case 'BUSD':
        return '$';
      case 'RUNE':
        return  'ᚱ '  ;
      case 'ASSET':
        return  this.utils.getAssetName(asset);
    }
  }

  getClassProfit(percent = '') {
    if (percent.length === undefined) {
      return 'text-success';
    }else {
      return percent.indexOf('-') ? 'text-success' : 'text-danger';
    }
  }


}
