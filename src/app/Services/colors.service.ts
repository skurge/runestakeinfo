import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ColorsService {

  constructor() { }

  colorDic = {
    'BNB.BNB': '#ffda00',
    'BNB.FTM-A64': '#25b6ea',
    'BNB.BUSD-BD1': '#8d6c02',
    RUNE: '#00f2c3',
    'BNB.AVA-645': '#3A3A78',
    'BNB.TWT-8C2': '#3375bb',
    'BNB.ETH-1C9': '#f3007a',
    'BNB.BTCB-1DE': '#ca7005',
    'BNB.CAN-677': '#33ccff',
    'BNB.BULL-BE4': '#ad4129',
    'BNB.DOS-120': '#3c3934',
    'BNB.SWINGBY-888': '#2100ae',
    'BNB.LTC-F07' : '',
    'BNB.CBIX-3C9' : '',
    'BNB.ERD-D06' : '',
    'BNB.DARC-24B' : '',
    'BNB.AWC-986' : '',
    'BNB.GIV-94E' : '',
    'BNB.LIT-099' : '',
    'BNB.BCH-1FD' : '#030e3c',
    'BNB.EOSBULL-F0D' : '#437399',
    'BNB.SHR-DB6' : '#0c61b5',
    'BNB.VIDT-F53' : '',
    'BNB.WISH-2D5' : '',
    'BNB.FRM-DE7' : '#6d5511',
    'BNB.ETHBULL-D33' : '#579200',
    'BNB.XRP-BF2' : '',
    'BNB.BEAR-14C' : '',
    'BNB.BZNT-464' : '#d0921d',
    'BNB.MITX-CAA' : '#7363ff',
    'BNB.BOLT-4C6' : '#3703d7',
    // 'BNB.PROPEL-6D9' : ''
  };

}
