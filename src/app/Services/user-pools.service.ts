import { Injectable } from '@angular/core';
import {ApiService} from './api-service.service';
import {AsgardConsumerApiService} from './asgard-consumer-api.service';
import {ChartService} from './chart.service';
import {BehaviorSubject, forkJoin, Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {DisplaySummary, ProfitSummary, StakePoolSummary} from '../models/staker-pool/StakePoolSummary';
import {AssetSummary} from '../models/staker-pool/assetSummary';
import BigNumber from 'bignumber.js';
import {AssetShare} from '../models/staker-pool/assetShare';
import {environment} from '../../environments/environment';
import {LoadingState} from '../models/loadingState';
import {StakersAssetData} from '../models/stakersAssetData';
import {PricesService} from './prices.service';
import {CalculationService} from './calculation.service';
import {UtilsService} from './utils.service';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class UserPoolsService {

  constructor(private calculationService: CalculationService,
              private utilsService: UtilsService) { }

  apiService: ApiService;
  asgardApiService: AsgardConsumerApiService;
  priceService: PricesService;

  poolList: Array<string> = [];
  loadingState: LoadingState = {message: '', state: true};

  triggerLoading = new BehaviorSubject(this.loadingState);
  poolListObj = new BehaviorSubject(this.poolList);

  userPools: StakersAssetData[];
  userPoolMap: {};
  generalPoolMap: {};

  stakeCurrentPoolSummaryMap: {[pool: string]: StakePoolSummary} = {};
  stakeHistoricalPoolSummaryMap: {[pool: string]: StakePoolSummary} = {};
  stakeCurrentPricePoolSummaryMap: {[pool: string]: StakePoolSummary} = {};
  displaySummary: DisplaySummary[] = [];
  assetShare: AssetShare[] = [];
  baseNumber = environment.BNB_BASE_NUMBER;

  totalLPercentage = '';
  totalLPNumber = new BigNumber(0);

  totalSummary: ProfitSummary = {
    baseTicker: '',
    totalPool: [],
    totalStake: [],
    totalWithdraw: [],
    totalGainLoss: [],
    percentageNumber: new BigNumber(0),
    percentage: '0'
  };

  lastLoaded: Date = new Date();

  showHideError(message, isShow) {
    this.loadingState.message = message;
    this.loadingState.state = isShow;
    this.triggerLoading.next(this.loadingState);
  }

  demoLogData(data){
    if (!environment.production){
      console.log(data);
    }
  }

  testLoading(){
    this.loadingState.message = 'dfgfdgdf';
    this.loadingState.state = true;
    this.triggerLoading.next(this.loadingState);
  }

  backgroundReload(isChaosnet: boolean, baseTicker: string){
    forkJoin([
      this.asgardApiService.getPools(this.poolList)
          .pipe(tap(data => {
            this.generalPoolMap = data.reduce(
                (dict, el, index) => (dict[el.asset] = el, dict),
                {}
            );
          //  console.log('backgroundReload getPools is working');
         //   console.log(this.generalPoolMap);
          })),
      this.priceService.getPrice(this.poolList.concat([isChaosnet ? 'BNB.BUSD-BD1' : 'BNB.BUSD-BAF']))
          .pipe(tap(data => {
         //   console.log('backgroundReload priceService.getPrice is working');
        //    console.log(data);
          })),
    ])
        .subscribe(results => {
       //   console.log('finish loading');
       //   console.log(results);
          this.loadPoolSumary(baseTicker);
        }, error => {
          this.demoLogData('Error getting user pool data.');
          this.showHideError('Something went wrong. Please try again.', true);
        });
  }

  resetLocalData() {
    this.userPools = [];
    this.userPoolMap = {};
    this.generalPoolMap = {};
    this.poolList = [];
    this.displaySummary = [];
  }

  loadUserPoolAndPool(address: string, isChaosnet: boolean, resetCheckBox: boolean, baseTicker: string) {
    // this.resetLocalData()
    this.asgardApiService.logUsage(resetCheckBox ? 'reset' : 'normal').subscribe(value => {
      // console.log(value);
    }, error => {
      // console.log(error);
    });
    this.apiService.getStaker(address).subscribe(stakerPool => {
      if (stakerPool.poolsArray === null) {
        this.demoLogData('Error getting user pool data.');
        this.showHideError('No pools data', true);
        return;
      }
      this.poolList = stakerPool.poolsArray;
      this.poolListObj.next(this.poolList);
      forkJoin([
        this.asgardApiService.getStakePools(address, stakerPool.poolsArray)
            .pipe(tap(data => {
              this.userPools = data;
              this.userPoolMap = this.userPools.reduce(
                  (dict, el, index) => (dict[el.asset] = el, dict),
                  {}
              );
           //   console.log('getStakePools is working');
          //    console.log(this.userPools);
            })),
        this.apiService.getPools(stakerPool.poolsArray)
            .pipe(tap(data => {
              this.generalPoolMap = data.reduce(
                  (dict, el, index) => (dict[el.asset] = el, dict),
                  {}
              );
           //   console.log('getPools is working');
           //   console.log(this.generalPoolMap);
            })),
        this.priceService.getPrice(stakerPool.poolsArray.concat([isChaosnet ? 'BNB.BUSD-BD1' : 'BNB.BUSD-BAF']))
            .pipe(tap(data => {
           //   console.log('priceService.getPrice is working');
           //   console.log(data);
            })),
        this.asgardApiService.getStakePoolSummary(address, this.poolList, resetCheckBox)
            .pipe(tap(data => {
              this.stakeHistoricalPoolSummaryMap = data.reduce(
                  (dict, el, index) => (dict[el.asset] = el, dict),
                  {}
              );
           //   console.log('getStakePoolSummary is working');
           //   this.demoLogData(this.stakeHistoricalPoolSummaryMap);
              // console.log(data);
            })),
      ])
          .subscribe(results => {
            console.log('finish loading');
         //   console.log(results);
            this.loadPoolSumary(baseTicker);
          }, error => {
            this.demoLogData('Error getting user pool data.');
            this.showHideError('Something went wrong. Please try again.', true);
          });

    }, error => {
      this.demoLogData('Error getting user pool data.');
      this.showHideError('Error getting pool data.', true);
    });
  }

  computePrice(isTarget: boolean, pool: string, baseTicker: string): string{
    return this.priceService.computePrice(isTarget, pool, baseTicker);
  }

  loadPoolSumary(baseTicker: string){
    this.demoLogData(this.userPoolMap);
    this.demoLogData(this.generalPoolMap);
  //  console.log('load pool')
    this.poolList.forEach(pool =>
    {
      const userStake = this.userPoolMap[pool];
      const generalPool = this.generalPoolMap[pool];
      const BUSD_PRICE = this.priceService.getBUSDPrice();
      this.demoLogData(userStake);
      this.demoLogData(generalPool);
      this.demoLogData(BUSD_PRICE);
      this.stakeCurrentPoolSummaryMap[pool] = this.calculationService.calculatePoolShareSummary(generalPool, userStake, BUSD_PRICE);
      const staker = this.stakeHistoricalPoolSummaryMap[pool];
      const stakerOriginaPool = _.cloneDeep(staker);
      this.demoLogData('########--- original');
      this.demoLogData(this.stakeHistoricalPoolSummaryMap[pool].stake.totalBUSDAmount.div(this.baseNumber).toFixed(2));
      this.demoLogData(this.stakeHistoricalPoolSummaryMap[pool].withdraw.totalBUSDAmount.div(this.baseNumber).toFixed(2));
      this.demoLogData(stakerOriginaPool.stake.totalBUSDAmount.div(this.baseNumber).toFixed(2));
      this.demoLogData(stakerOriginaPool.withdraw.totalBUSDAmount.div(this.baseNumber).toFixed(2));
      this.demoLogData('########');

      // tslint:disable-next-line:max-line-length
      this.stakeCurrentPricePoolSummaryMap[pool] = this.calculationService.calculateCurrentOriginalSummary(generalPool, Object.assign({}, stakerOriginaPool), BUSD_PRICE);
      this.demoLogData('########--after- original');
      this.demoLogData(this.stakeHistoricalPoolSummaryMap[pool].stake.totalBUSDAmount.div(this.baseNumber).toFixed(2));
      this.demoLogData(this.stakeHistoricalPoolSummaryMap[pool].withdraw.totalBUSDAmount.div(this.baseNumber).toFixed(2));
      this.demoLogData('########');
    });
    this.demoLogData(this.stakeCurrentPoolSummaryMap);

    this.demoLogData('\n\n\nCurrent Pool Share Amount');
    for (const [key, val] of Object.entries(this.stakeCurrentPoolSummaryMap)) {
      this.printSummary(val);
    }

    this.demoLogData('\n\n\nStake Original Stake Amount');
    for (const [key, val] of Object.entries(this.stakeHistoricalPoolSummaryMap)) {
      this.printSummary(val);
    }
    this.computeData(baseTicker);

  }

  computeData(baseTicker: string){
    this.demoLogData('########---first original');
    for (const [key, val] of Object.entries(this.stakeHistoricalPoolSummaryMap)) {
      this.printSummary(val);
    }
    this.demoLogData('########');
    const summary: DisplaySummary[] = [];
    const toalValueAdded: AssetSummary[] = [];
    const totalValueWithdraw: AssetSummary[] = [];
    const totalValueShare: AssetSummary[] = [];
    const totalGainLoss: AssetSummary[] = [];
    let totalPercent = new BigNumber(0);
    let toalValueAddedIL = new BigNumber(0);
    let toalValueWithdrawIL = new BigNumber(0);
    let totalValueShareIL = new BigNumber(0);

    const totalPercetString = '';

    const eachAsset: {[pool: string]: BigNumber} = {};
    eachAsset.RUNE = new BigNumber(0);

    this.poolList.forEach(pool => {
      const current = this.stakeCurrentPoolSummaryMap[pool];
      eachAsset[pool] = current.stake.assetAmount;
      eachAsset.RUNE = eachAsset.RUNE.plus(current.stake.targetAmount);

      const original = this.stakeHistoricalPoolSummaryMap[pool];
      this.demoLogData('########---summary original');
      for (const [key, val] of Object.entries(this.stakeHistoricalPoolSummaryMap)) {
        this.printSummary(val);
      }
      this.demoLogData('--------------------');
      this.demoLogData(original.stake.totalBUSDAmount.div(this.baseNumber).toFixed(2));
      this.demoLogData(original.withdraw.totalBUSDAmount.div(this.baseNumber).toFixed(2));
      this.demoLogData('########');
      const originalCurrent = this.stakeCurrentPricePoolSummaryMap[pool];
      const currentIL = this.calculationService.computeCurrentPercentage(current, originalCurrent);
      const profit = this.calculationService.calculateProfilt(original, current, this.utilsService.getAssetName(pool), baseTicker);
      if (baseTicker === 'ASSET'){
        const assetAdded: AssetSummary = {
          asset: pool,
          amount: profit.totalStake,
          percent: ''
        };
        toalValueAdded.push(assetAdded);

        const assetWithdraw: AssetSummary = {
          asset: pool,
          amount: profit.totalWithdraw,
          percent: ''
        };
        totalValueWithdraw.push(assetWithdraw);

        const assetShare: AssetSummary = {
          asset: pool,
          amount: profit.totalPool,
          percent: ''
        };
        totalValueShare.push(assetShare);
      }else {
        const currentValueAdded = toalValueAdded[0];
        if (currentValueAdded === undefined) {
          const assetAdded: AssetSummary = {
            asset: baseTicker,
            amount: profit.totalStake,
            percent: ''
          };
          toalValueAdded.push(assetAdded);
        }else{
          currentValueAdded.amount = currentValueAdded.amount.plus(profit.totalStake);
        }
        const currentValueShare = totalValueShare[0];
        if (currentValueShare === undefined) {
          const assetAdded: AssetSummary = {
            asset: baseTicker,
            amount: profit.totalPool,
            percent: ''
          };
          totalValueShare.push(assetAdded);
        }else{
          currentValueShare.amount = currentValueShare.amount.plus(profit.totalPool);
        }

        const currentValueWithdraw = totalValueWithdraw[0];
        if (currentValueWithdraw === undefined) {
          const assetWithdraw: AssetSummary = {
            asset: baseTicker,
            amount: profit.totalWithdraw,
            percent: ''
          };
          totalValueWithdraw.push(assetWithdraw);
        }else{
          currentValueWithdraw.amount = currentValueWithdraw.amount.plus(profit.totalWithdraw);
        }

      }

      this.demoLogData('each profit ' + profit.percentageNumber.toFixed(10));
      totalPercent = totalPercent.plus(profit.percentageNumber);
      toalValueAddedIL = toalValueAddedIL.plus(originalCurrent.stake.totalBUSDAmount);
      toalValueWithdrawIL = toalValueWithdrawIL.plus(originalCurrent.withdraw.totalBUSDAmount);
      totalValueShareIL = totalValueShareIL.plus(current.stake.totalBUSDAmount);

      const display: DisplaySummary = {
        pool,
        currentShare: current,
        original,
        current: currentIL,
        profit
      };
      summary.push(display);
      // this.demoLogData('\n\n\n ' + baseTicker);
      this.demoLogData('\n\n\n Calculate Profit ');

      // tslint:disable-next-line:max-line-length
      const percent1 = this.calculationService.calculatePercent(original.stake.totalAssetAmount, current.stake.totalAssetAmount.plus(original.withdraw.totalAssetAmount));
      this.demoLogData('\n ASSET ' + percent1[1]);
      // tslint:disable-next-line:max-line-length
      const percent2 = this.calculationService.calculatePercent(original.stake.totalTargetAmount, current.stake.totalTargetAmount.plus(original.withdraw.totalTargetAmount));
      this.demoLogData('\n RUNE ' + percent2[1]);
      // tslint:disable-next-line:max-line-length
      const percent3 = this.calculationService.calculatePercent(original.stake.totalBUSDAmount, current.stake.totalBUSDAmount.plus(original.withdraw.totalBUSDAmount));
      this.demoLogData('\n BUSD ' + percent3[1]);
    });

    const gainLossLP = this.calculationService.calculatePercent(toalValueAddedIL, totalValueShareIL.plus(toalValueWithdrawIL));
    this.totalLPNumber = gainLossLP[0];
    this.totalLPercentage = gainLossLP[2];

    this.displaySummary = summary;

    this.demoLogData('########');

    totalValueShare.forEach((value, index) => {
      this.demoLogData(index); // 0, 1, 2
      this.demoLogData(value); // 9, 2, 5
      const valueAdded = toalValueAdded[index];
      const valueWithdraw = totalValueWithdraw[index];
      this.demoLogData('valueAdded + ' + valueAdded.amount.div(this.baseNumber).toFixed(2));
      this.demoLogData('valueWithdraw + ' + valueWithdraw.amount.div(this.baseNumber).toFixed(2));
      this.demoLogData('totalValueShare + ' + value.amount.div(this.baseNumber).toFixed(2));
      const profit = this.calculationService.calculatePercent(valueAdded.amount, value.amount.plus(valueWithdraw.amount));
      this.demoLogData('profit + ' + profit[0].div(this.baseNumber).toFixed(2));
      const assetShare: AssetSummary = {
        asset: value.asset,
        amount: profit[0],
        percent: profit[2]
      };
      totalGainLoss.push(assetShare);
    });

    this.demoLogData('########');
    //
    this.totalSummary = {
      baseTicker: 'BNB',
      totalPool: totalValueShare,
      totalStake: toalValueAdded,
      totalWithdraw: totalValueWithdraw,
      totalGainLoss,
      percentageNumber: totalPercent.div(this.poolList.length),
      percentage: totalPercent.div(this.poolList.length).multipliedBy(100).toFixed(2) + '%'
    };
    //  }
    //
    const priceAsset: {[pool: string]: [BigNumber, BigNumber]} = {};
    let total = new BigNumber(0);

    this.demoLogData('calculate asset share: ');
    Object.keys(eachAsset).forEach(key => {
      const value = eachAsset[key];
      const price = this.priceService.getPriceBN(key);
      priceAsset[key] = [value, value.multipliedBy(price)];
      this.demoLogData('key ' + key + ' each: ' + price.toFixed(2) + ' amount: ' + value.div(this.baseNumber).toFixed(2) + ' price : ' + value.multipliedBy(price).div(this.baseNumber).toFixed(2));
      total = total.plus(value.multipliedBy(price));
    });

    const tempShare: AssetShare[] = [];

    Object.keys(priceAsset).forEach(key => {
      const value = priceAsset[key];
      const ration: AssetShare = {
        asset: key,
        ratio: (value[1].div(total)).multipliedBy(100).toNumber(),
        amount: value[0],
        price: value[1]
      };
      tempShare.push(ration);
    });

    this.assetShare = tempShare;
    this.lastLoaded = new Date();

    this.demoLogData(this.displaySummary);
    this.showHideError('', false);

  }

  printSummary(summary: StakePoolSummary){
    this.demoLogData(`asset : ${summary.asset} amount stake: ${summary.stake.assetAmount.div(this.baseNumber).toString()} targetAmount: ${summary.stake.targetAmount.div(this.baseNumber).toString()}`);
    this.demoLogData(`Converted to rune : ${summary.stake.totalTargetAmount.div(this.baseNumber).toString()} Converted to asset: ${summary.stake.totalAssetAmount.div(this.baseNumber).toString()} Converted to BUSD: ${summary.stake.totalBUSDAmount.div(this.baseNumber).toString()}`);


  }





}
