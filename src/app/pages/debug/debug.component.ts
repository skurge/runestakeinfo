import {Component, OnInit, OnDestroy, ViewChild, NgZone} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {HttpClient} from '@angular/common/http';
import BigNumber from 'bignumber.js';
import {forkJoin, interval, Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../Services/api-service.service';
import {StakersAssetData} from '../../models/stakersAssetData';
import {CalculationService} from '../../Services/calculation.service';
import {ImageNameService} from '../../Services/image-name.service';
import {AsgardConsumerApiService} from '../../Services/asgard-consumer-api.service';
import {DisplaySummary, ProfitSummary, StakePoolSummary} from '../../models/staker-pool/StakePoolSummary';
import {StorageMap} from '@ngx-pwa/local-storage';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';

import {WeeklyState} from '../../models/Asgard-consumer-model/WeeklyStateList';
import {PoolDetail} from '../../models/poolDetail';
import {environment} from '../../../environments/environment';
import {AssetShare} from '../../models/staker-pool/assetShare';
import {randomColor} from 'randomcolor';
import {AssetSummary} from '../../models/staker-pool/assetSummary';
import {ColorsService} from '../../Services/colors.service'; // import the script
import * as _ from 'lodash';
import {AnimationOptions} from 'ngx-lottie';
import {AnimationItem} from 'lottie-web';
import * as moment from 'moment';
import {UiServiceService} from '../../Services/ui-service.service';
import {skip, takeWhile, tap} from 'rxjs/operators';
import {PricesService} from '../../Services/prices.service';
import {ChartService} from '../../Services/chart.service';
import {UtilsService} from '../../Services/utils.service';
import {UserPoolsService} from '../../Services/user-pools.service';
@Component({
  selector: 'app-debug',
  templateUrl: './debug.component.html',
  styleUrls: ['./debug.component.scss']
})

export class DebugComponent implements OnInit, OnDestroy {
  constructor(private spinner: NgxSpinnerService,
              private http: HttpClient,
              private activeRoute: ActivatedRoute,
              private apiService: ApiService,
              private calculationService: CalculationService,
              private router: Router,
              private imageNameService: ImageNameService,
              private asgardApiService: AsgardConsumerApiService,
              private storage: StorageMap,
              private colorService: ColorsService,
              private ngZone: NgZone,
              private uiService: UiServiceService,
              private priceService: PricesService,
              public chartService: ChartService,
              private utilsService: UtilsService,
              public userPoolService: UserPoolsService) {
  }


  isChaosnet = true;
  activeClass = this.isChaosnet ? 'badge-info' : 'badge-neutral';
  address = '';
  unredactedAddresss = '';

  isShowAlert = false;
  errorMessage = 'Address not found.';

  baseNumber = Math.pow(10, 8); // 1e8

  isRedacted = false;
  hasQuery = false;
  copyButtonTest = 'Save URL';

  poolList: Array<string>;
  // new update

  assetColor: string[] = [];

  baseTicker = 'RUNE';
  resetCheckBox = false;

  // summaryBaseTicker = 'BUSD';
  subscription: Subscription;
  loadedTest = '';
  isLocalDbLoaded  = false;


  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  options: AnimationOptions = {
    path: '/assets/animation_new.json',
  };
  private animationItem: AnimationItem;
  animationCreated(animationItem: AnimationItem): void {
    this.animationItem = animationItem;
  //  console.log(animationItem);
  }

  getLastLoaded(value){
 //   console.log('reload : ' + value);
    this.loadedTest = moment(this.userPoolService.lastLoaded).fromNow();
   // this.userPoolService.backgroundReload(this.isChaosnet, this.baseTicker);
  }

  toggleVisibility(event){
   // console.log(this.resetCheckBox);
    this.storage.set('runestake_info_reset_mode', this.resetCheckBox).subscribe(() => {});
    this.showHideSpinner(false);
    // this.chartService.loadWeeklyState(this.address, this.poolList, this.baseTicker, this.assetColor, this.resetCheckBox);
    this.userPoolService.loadUserPoolAndPool(this.address, this.isChaosnet, this.resetCheckBox, this.baseTicker);
  }

  getBaseTicker(pool: string) {
    if (this.baseTicker === 'BUSD') {
      return this.baseTicker;
    } else if (this.baseTicker === 'RUNE') {
      return this.baseTicker;
    } else {
      return pool;
    }
  }

  changeConvert(ticker: string){
    this.baseTicker = ticker; // BUSD, RUNE, ASSET
    this.storage.set('runestake_info_base_ticker', ticker).subscribe(() => {});
    this.userPoolService.computeData(this.baseTicker);
    this.chartService.reloadWeeklyChart(this.baseTicker);
  }

  demoLogData(data){
    if (!environment.production){
      console.log(data);
    }
  }

  getImage(name) {
    return this.imageNameService.getImage(name);
  }

  redactToggle(state) {
    this.isRedacted = state;
    if (state) {
      this.unredactedAddresss = this.address;
      this.address = '⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡';
    } else {
      this.address = this.unredactedAddresss;
    }
  }

  generateColor(count: number) {
    this.assetColor.push(this.colorService.colorDic.RUNE);
    this.poolList.forEach(value => {
      const color = this.colorService.colorDic[value];
      if (color === undefined || color === ''){
        this.assetColor.push( randomColor({
          dark: true,
        }));
      }else {
        this.assetColor.push(color);
      }
    });
  }

  showHideError(message, isShow) {
    this.showHideSpinner(true);
    this.isShowAlert = isShow;
    this.errorMessage = message;
  }

  getBaseUrl(): string {
    return this.isChaosnet ? 'https://chaosnet-midgard.bepswap.com' : 'https://midgard.bepswap.com';
  }
  getColumnClass(){
   return this.uiService.getColumnClass(this.userPoolService.displaySummary.length);
  }

  ngOnInit() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('index-page');
    this.apiService.baseUrl = this.getBaseUrl();
    this.priceService.asgardService = this.asgardApiService;
    this.priceService.apiService = this.apiService;
    this.chartService.asgardApiService = this.asgardApiService;
    this.chartService.calculationService = this.calculationService;
    this.userPoolService.apiService = this.apiService;
    this.userPoolService.asgardApiService = this.asgardApiService;
    this.userPoolService.priceService = this.priceService;

 //   console.log(this.userPoolService.loadingState);
    this.userPoolService.triggerLoading.pipe(skip(1)).subscribe(value => {
   //   console.log('testing subscribe event');
   //   console.log(value);
   //   console.log(this.userPoolService.displaySummary);
      this.getLastLoaded(1);
      this.showHideError(value.message, value.state);
    });

    this.userPoolService.poolListObj.pipe(skip(1)).subscribe(value => {
     // console.log('testing poolListObj event');
      this.onGetPoolList(value);
    });

    // This is METHOD 1
    const source = interval(30000);
    // const text = 'Your Text Here';
    this.subscription = source.subscribe(val => this.getLastLoaded(val));


    this.activeRoute.queryParams.subscribe(params => {
   //  console.log('recied event query param');
      // this.isChaosnet = true;
      this.address = params.address;
   //  console.log(this.address);
      if (this.address !== undefined && this.address.length > 20) {
        this.hasQuery = true;
     //   console.log('isLoaded: ' + this.isLocalDbLoaded);
        if (this.isLocalDbLoaded){
          this.testEvent();
        }
      }else {
        this.hasQuery = false;
        this.address = '';
      }
    });

    forkJoin([
      this.storage.get('runestake_info_base_ticker')
          .pipe(tap(data => {
            this.baseTicker = data !== undefined ? `${data}` : 'BUSD';
        //    console.log('runestake_info_base_ticker is working');
            console.log(data);
          })),
      this.storage.get('runestake_info_reset_mode')
          .pipe(tap(data => {
            this.resetCheckBox = data === true ? true : false;
        //    console.log('runestake_info_reset_mode is working');
         //   console.log(data);
          })),
    ])
        .subscribe(results => {
       //   console.log('finish loading');
       //   console.log(results);
          this.isLocalDbLoaded = true;
          if (this.hasQuery){
            this.testEvent();
          }
        }, error => {
          this.demoLogData('Error getting user pool data.');
          this.showHideError('Something went wrong. Please try again.', true);

        });

    // this.storage.get('runestake_info_base_ticker').subscribe((user: string ) => {
    //   this.demoLogData('user convet asset ' + user);
    //   this.baseTicker = user !== undefined ? user : 'BUSD';
    // });

    // this.storage.get('runestake_info_reset_mode').subscribe((mode: boolean ) => {
    //   this.demoLogData('runestake_info_reset_mode' + mode);
    //   this.resetCheckBox = mode === true ? true : false;
    //   this.demoLogData(this.resetCheckBox);
    // });






  }

  onGetPoolList(poolList: string[]){
    this.poolList = poolList;
    this.generateColor(this.poolList.length);
    console.log('on pool change :' + Date())
     this.chartService.loadWeeklyState(this.address, this.poolList, this.baseTicker, this.assetColor, this.resetCheckBox);
  }

  ngOnDestroy() {
    // console.log('destroy event');
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('index-page');
    // tslint:disable-next-line:no-unused-expression
    // this.userPoolService.triggerLoading && this.userPoolService.triggerLoading.unsubscribe();
    // // tslint:disable-next-line:no-unused-expression
    // this.userPoolService.poolListObj && this.userPoolService.poolListObj.unsubscribe();
    // // tslint:disable-next-line:no-unused-expression
    // this.subscription && this.subscription.unsubscribe();

  }

  chaosNetClick(isChaosNet) {
    this.demoLogData('click' + isChaosNet);
    this.address = '';
    this.isChaosnet = isChaosNet;
    this.demoLogData('click' + isChaosNet);
    this.apiService.baseUrl = this.getBaseUrl();
  }

  search() {
    if (this.address.length < 10) {
      this.showHideError('Please double check your address ', true);
    } else {
      this.showHideError('', false);
      this.testEvent();
    }

  }

  onKey(event: KeyboardEvent) {
    this.showHideError('', false);
    if (event.key === 'Enter') {
      // this.loadData();
      this.testEvent();
    }
  }

  showHideSpinner(isHide) {
    if (this.animationItem !== undefined) {
      if (isHide) {
        this.stop();
      }else {
        this.play();
      }
    }
    !isHide ? this.spinner.show() : this.spinner.hide();
  }

  testEvent() {
    if (this.address !== undefined){
      this.showHideSpinner(false);
      this.loadEvent();
    }
    if (!this.hasQuery) {
      this.demoLogData('prepare routing');
      this.router.navigateByUrl('/home?address=' + this.address);
      return;
    }else {
      this.demoLogData('no routing needed');
    }

  }

  resetLocalData() {
    this.userPoolService.resetLocalData();
    this.poolList = [];
  }

  loadEvent(isReload: boolean = false) {
    if (!isReload){
      this.resetLocalData();
    }
    this.showHideSpinner(false);
    // console.log('loading for ticker' + this.baseTicker);

    this.userPoolService.loadUserPoolAndPool(this.address, this.isChaosnet, this.resetCheckBox, this.baseTicker);
  }

  reloadData(){
    this.loadEvent(true);
  }

  getBaseTotalSummary(asset){
    return this.uiService.getBaseTotalSummary(asset, this.baseTicker);
  }
  addCurrentSign(sign: string, value: string): string {
    return  this.utilsService.addCurrentSign(sign, value);
  }



  getBase(base: string): string{
   return  this.utilsService.getAssetName(base);
  }

  getClassProfit(percent = '') {
    return this.uiService.getClassProfit(percent);
  }

  toString(value: BigNumber, needPrefix: boolean = false, pool: string = '',
           needBase: boolean = true, isPrice: boolean = false, isUSD: boolean = false): string{
    // console.log(value);
    // console.log(pool);
    if (value === undefined){
      return  'undefined';
    }
    return this.utilsService.toString(this.baseTicker, value, needPrefix, pool, needBase, isPrice, isUSD);
  }



  onCopiedSuccess(event){
    //    this.demoLogData(event);
    if (event.isSuccess) {
      this.copyButtonTest = 'Address Copied.';
    }else {
      this.copyButtonTest = 'Unable to copied.';
    }
    setTimeout(() => {
      this.copyButtonTest = 'Save URL';
    }, 20000 / 60);
  }

  getDemoAddressURL(): string{
    return 'https://runestake.info/home?address=' +  this.address;
  }


  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    this.demoLogData(active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    this.demoLogData(active);
  }

  public hideOne(): void {
    const isHidden = this.chart.isDatasetHidden(1);
    this.chart.hideDataset(1, !isHidden);
  }


  getWidth(assetShare: AssetShare, index: number) {
    const styles = {
      width: assetShare.ratio + '%',
      background: this.assetColor[index]
    };
    return styles;
  }

  setCircleColor(index: number){
    const styles = {
      color: this.assetColor[index]
    };
    return styles;
  }

  setMarginTop(index: number) {
    const styles = {
      'margin-top': index === 0 ? '0px' : '-20px'
    };
    return styles;
  }



  stop(): void {
    console.log('stop');
    this.ngZone.runOutsideAngular(() => this.animationItem.stop());
  }

  play(): void {
    this.ngZone.runOutsideAngular(() => this.animationItem.play());
  }

  //  APY = (1 + LP vs HODL % / (total days / 7 day per week)) ^ 52 -1
  calculateAPY(time: string, percentageNumber: BigNumber): string {
    const dayCount = this.calculateTime(time);
    if (dayCount < 7) {
      return 'LP vs HODL APY shows after 7 days';
    }
    const numberOfWeek = dayCount / 7 ;
    const periodicRatePercent = percentageNumber.div(numberOfWeek);
    const periodicRate = periodicRatePercent.div(100);
    const apy = (((periodicRate.plus(1)).pow(52)).minus(1)).multipliedBy(100);
    return apy.toFixed(2) + '%';
  }

  getClassAPYProfit(time: string, percent: string): string{
    if (this.calculateTime(time) >= 7 ){
      return '';
    }
    return  this.getClassProfit(percent);
  }


  dayCount(time: string): string{
    const count = this.calculateTime(time);
    const s = new Date(parseInt(time) * 1000);
    return `${count} ${count === 1 ? 'day' : 'days'} (${s.toLocaleDateString()}) `;
  }
  calculateTime(time: string): number{
    // this.demoLogData('########time' + time);
    const s = new Date(parseInt(time) * 1000);
    const diff = this.dayDiff(s, new Date());
    return diff;
  }

  // Calculate the difference of two dates in total days
  //  diffDays(d1, d2):number
  dayDiff(d1: Date, d2: Date): number
  {
    const diff = Math.abs(d1.getTime() - d2.getTime());
    const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
    return diffDays;
  }

}
